package com.victormiranda.assemblr.core;

import com.victormiranda.assemblr.annotations.AssemblerMapping;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author victormiranda@gmail.com
 */
public class FieldMatcher {

    private final static Logger LOG = LogManager.getLogger(FieldMatcher.class.getName());

    public List<FieldMatch> matchesBetween(final Class sourceClass, final Class destinationClass) {
        final List<FieldMatch> matchesBetween = new ArrayList<>();
        final Field[] destinationFields = ReflectionUtil.getClassFields(destinationClass);

        final List<FieldMatch> mappingMatches = new ArrayList<>();

        return matchesBetween;
    }
}
