package com.victormiranda.assemblr.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

/**
 * @author victormiranda@gmail.com
 */
public class FieldMatch {
    private final Field originField;
    private final Field destinationField;
    private final String mapping;

    private final static Logger LOG = LogManager.getLogger(FieldMatch.class.getName());

    public FieldMatch(Field originField, Field destinationField, String mapping) {
        this.originField = originField;
        this.destinationField = destinationField;
        this.mapping = mapping;
    }

    public Field getDestinationField() {
        return destinationField;
    }

    public Field getOriginField() {
        return originField;
    }

    public String getMapping() {
        return mapping;
    }

}
