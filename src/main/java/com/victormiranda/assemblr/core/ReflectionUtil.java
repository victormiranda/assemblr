package com.victormiranda.assemblr.core;

import com.sun.istack.internal.Nullable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.util.*;

/**
 * @author victormiranda@gmail.com
 */
public final class ReflectionUtil {
    private final static Logger LOG = LogManager.getLogger(ReflectionUtil.class.getName());

    private final static Map<Class, Class> PRIMITIVE_TYPES = getPrimitiveTypes();
    private final static Map<Class, ArrayList<Class>> PRIMITIVE_BOXINGS = getPrimitiveBoxings();

    public static Field[] getClassFields(final Class clazz) {
        return clazz.getDeclaredFields();
    }

    public static Optional<Field> getMatchingField(final Class destinationClass, final Field originField) {
        Field destinationField = null;

        try {
            final Field candidateField = destinationClass.getDeclaredField(originField.getName());
            final boolean isAssignable = isAssignable(candidateField.getType(), originField.getType());

            if (isAssignable) {
                destinationField = candidateField;
            }
        } catch (NoSuchFieldException e) {
            LOG.info("Destinationclass " + destinationClass +" doesn't have field " + originField.getName());
        }

        return Optional.ofNullable(destinationField);
    }

    public static Optional<Field> getNestedField(final Class clazz, final String mapping) {
        final Field nestedField = getRecursiveNestedField(clazz, mapping);

        return Optional.ofNullable(nestedField);
    }

    @Nullable
    private static Field getRecursiveNestedField(final Class clazz, final String mapping) {
        Field nestedField = null;
        final int firstPointPosition = mapping.indexOf(".");

        try {
            if (firstPointPosition == -1) {
                nestedField = clazz.getDeclaredField(mapping);
            } else {
                final Class newNestedType = clazz.getDeclaredField(mapping.substring(0, firstPointPosition)).getType();
                nestedField = getRecursiveNestedField(newNestedType, mapping.substring(firstPointPosition + 1));
            }

        } catch (NoSuchFieldException e) {
            LOG.info("class " + clazz +" doesn't have map " + mapping);
        }

        return nestedField;
    }

    private static boolean isAssignable(final Class origin, final Class destination) {
        final boolean isPrimitiveOrigin = isPrimitive(origin);
        final boolean isPrimitiveDestination = isPrimitive(destination);

        if (!isPrimitiveOrigin && !isPrimitiveDestination) {
            return destination.isAssignableFrom(origin);
        } else if (isPrimitiveOrigin && isPrimitiveDestination) {
            LOG.info("Interprimitive assignment");
            return PRIMITIVE_BOXINGS.get(destination).contains(origin);
        } else if (isPrimitiveOrigin) {
            return isAssignable(destination, origin);
        } else if(isPrimitiveDestination) {
            return PRIMITIVE_TYPES.get(destination) == origin;
        }

        return false;
    }

    private static boolean isPrimitive(final Class type) {
        return PRIMITIVE_TYPES.get(type) != null;
    }

    private static Map<Class,Class> getPrimitiveTypes() {
        final Map<Class, Class> ret = new HashMap<>();

        ret.put(boolean.class, Boolean.class);
        ret.put(Character.class, Character.class);
        ret.put(byte.class, Byte.class);
        ret.put(short.class, Short.class);
        ret.put(int.class, Integer.class);
        ret.put(long.class, Long.class);
        ret.put(float.class, Float.class);
        ret.put(double.class, Double.class);

        return ret;
    }

    private static Map<Class, ArrayList<Class>> getPrimitiveBoxings() {
        final Map<Class, ArrayList<Class>> ret = new HashMap<>();

        ret.put(byte.class,new ArrayList<Class>(Arrays.asList(byte.class)));
        ret.put(short.class,new ArrayList<Class>(Arrays.asList(short.class, byte.class)));
        ret.put(int.class,new ArrayList<Class>(Arrays.asList(int.class, short.class, byte.class)));
        ret.put(long.class,new ArrayList<Class>(Arrays.asList(long.class, int.class, short.class,  byte.class)));
        ret.put(float.class,new ArrayList<Class>(Arrays.asList( float.class, long.class, int.class, short.class,  byte.class)));
        ret.put(double.class,new ArrayList<Class>(Arrays.asList(double.class, float.class, long.class, int.class, short.class,  byte.class)));

        return ret;
    }
}
