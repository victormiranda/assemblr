package com.victormiranda.assemblr.core;

import com.victormiranda.assemblr.annotations.AssemblerConvertibleTo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * @author victormiranda@gmail.com
 */
public class Assembler {

    private final FieldMatcher fieldMatcher;

    public Assembler() {
        fieldMatcher = new FieldMatcher();
    }

    private final static Logger LOG = LogManager.getLogger(Assembler.class.getName());

    public Object map(final Object source, final Class destinationClass) {
        final Class sourceClass = source.getClass();
        final List<FieldMatch> matchList = fieldMatcher.matchesBetween(sourceClass, destinationClass);

        Object destinationObject = null;

        try {
            destinationObject = destinationClass.newInstance();
        } catch (InstantiationException| IllegalAccessException e) {
            LOG.error("WTF");
        }

        return destinationObject;
    }

    public Object map(final Object source) {
        final AssemblerConvertibleTo convertedToAnnotation =
                source.getClass().getAnnotation(AssemblerConvertibleTo.class);

        if (convertedToAnnotation == null) {
            throw new IllegalArgumentException();
        }

        return map(source, convertedToAnnotation.value());
    }


}
