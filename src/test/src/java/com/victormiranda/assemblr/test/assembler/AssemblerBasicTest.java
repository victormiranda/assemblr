package com.victormiranda.assemblr.test.assembler;

import com.victormiranda.assemblr.core.Assembler;
import com.victormiranda.assemblr.test.model.Country;
import com.victormiranda.assemblr.test.model.CountryDTO;
import junit.framework.Assert;
import org.junit.Test;

/**
 * @author victormiranda@gmail.com
 */
public class AssemblerBasicTest {

    private final Assembler assembler = new Assembler();

    @Test
    public void testBasicAssembly() {
        final Country country = new Country();

        country.setId(1);
        country.setIso2("ei");
        country.setName("Ireland");

        final CountryDTO countryDTO = (CountryDTO) assembler.map(country, CountryDTO.class);

        Assert.assertTrue(country.getId().equals(countryDTO.getId()));
        Assert.assertTrue(country.getIso2().equals(countryDTO.getIso2()));
        Assert.assertTrue(country.getName().equals(countryDTO.getName()));
    }
}
