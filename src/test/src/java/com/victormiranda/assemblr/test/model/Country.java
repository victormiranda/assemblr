package com.victormiranda.assemblr.test.model;

import com.victormiranda.assemblr.annotations.AssemblerConvertibleTo;

/**
 * @author victormiranda@gmail.com
 */
@AssemblerConvertibleTo(CountryDTO.class)
public class Country {
    private Integer id;
    private String iso2;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIso2() {
        return iso2;
    }

    public void setIso2(String iso2) {
        this.iso2 = iso2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
